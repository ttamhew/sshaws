
from cement import App, TestApp, init_defaults
from cement.core.exc import CaughtSignal
from .core.exc import SshAwsError
from .controllers.base import Base
from .controllers.list import List
from .controllers.add import Add
from .controllers.connect import Connect
from .controllers.ssh_config import SshConfig
from .hooks.initialize import sshaws_initialize
from .core.ec2 import Ec2
from .core.regions import Regions
from .core.ssh_connections import SshConnections

from .core.ssh_config import Ssh_Config
from pathlib import Path



class SshAws(App):
    """SSH AWS primary application."""

    class Meta:
        label = 'sshaws'

        # call sys.exit() on close
        exit_on_close = True

        # load additional framework extensions
        extensions = [
            'yaml',
            'colorlog',
            'jinja2',
            'tabulate',
        ]

        # config files
        config_files = [
            str(Path.home() / '.sshaws/config/sshaws.yaml')
        ]

        # configuration handler
        config_handler = 'yaml'

        # configuration file suffix
        config_file_suffix = '.yml'

        # set the log handler
        log_handler = 'colorlog'

        # set the output handler
        output_handler = 'tabulate'
        
        # used with jinja2
        # template_dirs = ['./templates']

        # register handlers
        handlers = [
            Base,
            List,
            Add,
            Connect
        ]

        # Hooks
        hooks = [
            ('post_setup', sshaws_initialize)
        ]


class SshAwsTest(TestApp,SshAws):
    """A sub-class of SshAws that is better suited for testing."""

    class Meta:
        label = 'sshaws'


def main():
    with SshAws() as app:
        try:
            app.args.add_argument('-r', '--region', 
                          action='store', 
                          dest='aws_region')

            app.regions = Regions(app)
            app.ec2 = Ec2(app)
            app.connections = SshConnections(app, Path.home().resolve() / '.sshaws/config/sshaws_connection_config.yaml')

            app.run()

        except AssertionError as e:
            print('AssertionError > %s' % e.args[0])
            app.exit_code = 1

            if app.debug is True:
                import traceback
                traceback.print_exc()

        except SshAwsError as e:
            print('SshAwsError > %s' % e.args[0])
            app.exit_code = 1

            if app.debug is True:
                import traceback
                traceback.print_exc()

        except CaughtSignal as e:
            # Default Cement signals are SIGINT and SIGTERM, exit 0 (non-error)
            print('\n%s' % e)
            app.exit_code = 0

if __name__ == '__main__':
    main()
