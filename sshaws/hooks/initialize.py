import yaml
import boto3
import inquirer
from ..core.exc import SshAwsError
from pathlib import PosixPath, Path
from cement import shell

def sshaws_initialize(app):

    default_ssh_connection_config = Path.home().resolve() / '.sshaws/config/sshaws_connection_config.yaml'

    if 'app_initialized' not in app.config.keys('sshaws'):

        confirm = inquirer.confirm("Do you want to initialize sshaws?", default=True)

        if not confirm:
            app.log.warning("sshaws must be initialized before use.")
            app.close()
        
        responses = ask_initilization_questions(app, default_ssh_connection_config)

        config = {
            'sshaws': {
                'app_initialized': True,
                'default_regions': responses['default_regions'],
                'ssh_connection_config': PosixPath(responses['ssh_connection_config']).expanduser().as_posix(),
                'ssh_keys_directory': PosixPath(responses['ssh_keys_directory']).expanduser().as_posix(),
            }
        }

        if len(responses['default_regions']) == 0:
            app.log.warning("SSHAWS not initialized.")
            app.close()

        Path(Path.home() / ".sshaws/config").mkdir(parents=True, exist_ok=True)

        with open(str(Path.home() / '.sshaws/config/sshaws.yaml'), 'w') as outfile:
            yaml.dump(config, outfile, default_flow_style=False)

        app.config.merge(config)

    return True

def ask_initilization_questions(app, default_ssh_connection_config):
    # Get AWS regions
    app.log.debug("Getting all regions that work with EC2...")

    ec2 = boto3.client('ec2')
    regions = [region['RegionName'] for region in ec2.describe_regions()['Regions']]

    questions = [
        inquirer.Checkbox('default_regions',
            message="Select you default AWS region or regions",
            choices=regions,
            validate=validate_region_answers
        ),
        inquirer.Path(
            'ssh_keys_directory', 
            '~/.ssh/keys', 
            message="SSH keys directory:",
            path_type=inquirer.Path.DIRECTORY,
            exists=True,
            normalize_to_absolute_path=True,
        ),
        inquirer.Path(
            'ssh_connection_config',
            default_ssh_connection_config,
            message="Select your SSHAWS connection configuration file",
            path_type=inquirer.Path.FILE,
            exists=False,
            normalize_to_absolute_path=True,
        ),
    ]
    responses = inquirer.prompt(questions)

    app.log.debug("Default regions: " + str(responses['default_regions']))
    app.log.debug("SSH connection config: " + str(responses['ssh_connection_config']))
    app.log.debug("SSH Keys Directory: " + str(responses['ssh_keys_directory']))

    return responses


def validate_region_answers(answers, current):
    if not current:
        raise inquirer.errors.ValidationError('', reason='You must choose at least one region.')
    else:
        return True
