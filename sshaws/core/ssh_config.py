from sshconf import read_ssh_config, SshConfig
from pathlib import Path
from ..core.exc import SshAwsError
from ..core.ec2 import Ec2
import stringcase
import os

class SshawsConfigurationObject:

    def __init__(self, instance, user, port, key, aws_region):
        self.instance = instance
        self.user = user
        self.port = port
        self.key = key
        self.aws_region = aws_region
        self.ssh_host = self.get_ssh_host()

    def get_ssh_host(self):
        for tags in self.instance.tags:
            if tags["Key"] == 'Name':
                name = tags["Value"]
                break
        
        if name is None:
            raise SshAwsError("EC2 instance has no Name tag.")

        name = stringcase.lowercase(name)
        name = stringcase.alphanumcase(name)

        return self.aws_region + ':' + name


class Ssh_Config:

    def __init__(self, app):
        self.app = app

    def add(self, ssh_configuration: SshawsConfigurationObject):
        self.ssh_configuration = ssh_configuration

        # Read the ssh config
        self.read_ssh_config()

        # Get SSH config for host
        config = self.ssh_config.host(self.ssh_configuration.ssh_host)

        if not config:
            self.ssh_config.add(
                self.ssh_configuration.ssh_host,
                Hostname=self.ssh_configuration.instance.public_ip_address,
                Port=self.ssh_configuration.port,
                User=self.ssh_configuration.user,
                IdentityFile=self.ssh_configuration.key.resolve()
            )
            self.ssh_config.save()
        else:
            self.ssh_config.set(
                self.ssh_configuration.ssh_host,
                Hostname=self.ssh_configuration.instance.public_ip_address,
                Port=self.ssh_configuration.port,
                User=self.ssh_configuration.user,
                IdentityFile=self.ssh_configuration.key.resolve()
            )
            self.ssh_config.save()

        return self.ssh_configuration.ssh_host


    def read_ssh_config(self):
        try:
            ssh_config_path = Path(self.app.config.get('sshaws', 'ssh_config_file'))

            if not ssh_config_path.is_file():
                self.app.log.error("SSH config not found: %s" % ssh_config_path.resolve())
                raise SshAwsError("SSH config not found")

            self.ssh_config = read_ssh_config(ssh_config_path.resolve())

        except:
            self.app.log.error("Error reading SSH config: %s" % ssh_config_path.resolve())
            raise SshAwsError("Error reading SSH config")

    
    def exist_ssh_host(self, host, region=None, convert_from_instance_name = True):
        # Read the ssh config
        self.read_ssh_config()

        if convert_from_instance_name:
            host = self.convert_to_ssh_host(host)

        if region is not None:
            host = region + ':' + host

        return host in self.ssh_config.hosts()


