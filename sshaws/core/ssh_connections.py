from pathlib import Path
import yaml
import uuid
from ..core.exc import SshAwsError


class Ec2SshConnection:

    def __init__(self, **kwargs):

        self._aws_region = kwargs.get('region')
        self._user = kwargs.get('user')
        self._port = kwargs.get('port')
        self._port = Path(kwargs.get('key'))

        print(kwargs.keys())

        if kwargs.

        # if not ec2_instance:
        #     self._name = self.get_instance_name(ec2_instance)
        #     self._instance_id = self.get_instance_id(ec2_instance)
        #     self._hostname = self.get_instance_hostname(ec2_instance)


    @staticmethod
    def get_instance_name(ec2_instance):
        for tags in ec2_instance.tags:
            if tags["Key"] == 'Name':
                return tags["Value"]
        raise SshAwsError("EC2 instance has no name (%s)." % ec2_instance.id)

    @staticmethod
    def get_instance_hostname(ec2_instance):
        if ec2_instance.public_ip_address:
            return ec2_instance.public_ip_address
        raise SshAwsError("EC2 instance has no ip (%s)." % ec2_instance.public_ip_address)

    @staticmethod
    def get_instance_id(ec2_instance):
        if ec2_instance.id:
            return ec2_instance.id
        raise SshAwsError("EC2 instance has no id (%s)." % ec2_instance.id)

    def get_identifier(self):
        return self._aws_region + ':' + self._name


class SshConnections:

    def __init__(self, app, ssh_connections_path):
        self._app = app

        self.check_ssh_connections_path(ssh_connections_path)
        self._ssh_connections_path = ssh_connections_path
        self._ssh_connections = {}

    @property
    def ssh_connections_path(self):
        return self._ssh_connections_path

    def check_ssh_connections_path(self, ssh_connections_path):
        if not Path(ssh_connections_path).exists():
            self._app.log.debug('SSH connections file not found, creating: %s' % ssh_connections_path)

            data = {
                'connections': []
            }

            with open(ssh_connections_path, 'w') as outfile:
                yaml.dump(data, outfile, default_flow_style=False)

        return True

    def load_ssh_connections_file(self):
        with open(self._ssh_connections_path) as file:
            raw_ssh_connections = yaml.load(file, Loader=yaml.FullLoader)
            for connection in raw_ssh_connections['connections']:
                self._ssh_connections = Ec2SshConnection(
                    region=connection['region'],
                    user=connection['user'],
                    port=connection['port'],
                    key=connection['key']
                )
                print(connection)

    def add(self, instance_name, instance_id, instance_region, ssh_hostname, ssh_user, ssh_port, ssh_key):
        self.load_ssh_connections_file()

        # check for a matching connection


        connection = {
            'id': str(uuid.uuid4()),
            'name': instance_name,
            'instance_id': instance_id,
            'region': instance_region,
            'hostname': ssh_hostname,
            'user': ssh_user,
            'port': int(ssh_port),
            'key': str(ssh_key),
        }

        # Check key
        self.connection_exists(instance_region, instance_name)




        # self._ssh_connections['connections'].append(connection)
        #
        # with open(self._ssh_connections_path, 'w') as outfile:
        #     yaml.dump(self._ssh_connections, outfile, default_flow_style=False)


    def connection_exists(self, instance_region, instance_name):
        connections = []







