class Regions:

    def __init__(self, app):
        self.app = app
        self.default_regions = self.app.config.get('sshaws', 'default_regions')

    def get(self):
        
        if self.app.pargs.aws_region:
            return [self.app.pargs.aws_region]
        else:
            return self.default_regions