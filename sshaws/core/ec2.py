import boto3

class Ec2:

    def __init__(self, app):
        self.app = app
        self.default_regions = self.app.config.get('sshaws', 'default_regions')
        self.instance_collections = {}

    def get_instance_collections(self, fresh = False):
        if fresh:
            self.instance_collections = {}
        else:
            if self.instance_collections:
                return self.instance_collections

        for region in self.app.regions.get():
            
            self.instance_collections[region] = []

            self.app.log.debug("Setting aws region " + region)
            boto3.setup_default_session(region_name=region)

            self.app.log.debug("Getting list of ec2 instances in the " + region + " region")
            ec2 = boto3.resource('ec2')
        
            instances = ec2.instances.filter(
                Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
        
            for instance in instances:
                self.instance_collections[region].append(instance)

        return self.instance_collections

    def get_ec2_instance_tag_name(self, ec2_tags):
        for tags in ec2_tags:
            if tags["Key"] == 'Name':
                instancename = tags["Value"]
        return instancename