from cement import Controller, ex
import boto3

class Connect(Controller):
    class Meta:
        label = 'connect'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(help='Connect to a EC2 instance.')
    def connect(self):
        self.valid_ssh_hosts = {}

        # Get ec2 instances
        self.instance_collections = self.app.ec2.get_instance_collections()

        for region, instance_collection in self.instance_collections.items():
            self.valid_ssh_hosts[region] = {}
            for instance in instance_collection:
                name = self.app.ec2.get_ec2_instance_tag_name(instance.tags)
                if self.app.ssh_config.exist_ssh_host(name, region):
                    self.valid_ssh_hosts[region][name] = instance
        
        print(self.valid_ssh_hosts)