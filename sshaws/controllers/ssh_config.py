from sshconf import read_ssh_config
from os.path import expanduser
from cement import Controller, ex
import boto3
import stringcase

class SshConfig(Controller):    
    class Meta:
        label = 'ssh-config'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(help='Print current SSH Config')
    def ssh_config(self):
        
        config = read_ssh_config(expanduser("~/.ssh/config"))

        data = []

        for host in config.hosts():
            host_data = config.host(host)
            data.append([
                host, 
                host_data.get('hostname', ''), 
                host_data.get('port', ''),
                host_data.get('identityfile', '')
                ])  

        headers = [ 'Host', 'Hostname', 'Port', 'User', 'Identity File']

        self.app.render(data, headers=headers)
        pass

    @ex(
        help='Add/Update ssh config entry for a EC2 instance.',
        # sub-command level arguments. ex: 'sshaws ssh-add --foo bar'
        arguments=
        [
            ### add a sample foo option under subcommand namespace
            ( [ '-n', '--instance-name' ],
              { 
                'help': 'EC2 Instance Name',
                'action': 'store',
                'dest': 'ec2_name'
              } ),
        ])
    def ssh_add(self):
        
        boto3.setup_default_session(region_name=self.app.pargs.aws_default_region)
        ec2 = boto3.resource('ec2')
        instances = ec2.instances.filter(
            Filters=[
                {'Name': 'instance-state-name', 'Values': ['running']},
                {'Name':'tag:Name', 'Values':[self.app.pargs.ec2_name]}
            ])


        if len([instance for instance in instances]) > 1:
            print("More than one EC2 instance with that name found.")
            return
        
        for instance in instances:
            ec2_instance = instance

        # SSH Config
        ssh_config = read_ssh_config(expanduser("~/.ssh/config"))
        config = ssh_config.host("self.app.pargs.ec2_name")

        if not config:
            ssh_config.add(self.ssh_host_encode(self.app.pargs.ec2_name), Hostname=ec2_instance.public_ip_address, Port=22, User="admin")
            ssh_config.save()

        data = []

        for instance in instances:
            data.append([
                self.get_ec2_instance_name(instance.tags),
                self.ssh_host_encode(self.app.pargs.ec2_name), 
                ec2_instance.public_ip_address, 
                "ssh " + self.ssh_host_encode(self.app.pargs.ec2_name),     
                ])


        headers = [ 'EC2 Instance Name', 'SSH Host', 'Hostname', 'SSH Command']

        self.app.render(data, headers=headers)

        pass

    def get_ec2_instance_name(self, ec2_tags):
        for tags in ec2_tags:
            if tags["Key"] == 'Name':
                instancename = tags["Value"]
        return instancename

    
    def ssh_host_encode(self, host):
        host = stringcase.lowercase(host)
        return stringcase.alphanumcase(host)


