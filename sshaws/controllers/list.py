from cement import Controller, ex
import os
import boto3

class List(Controller):
    class Meta:
        label = 'list'
        stacked_type = 'embedded'
        stacked_on = 'base'

    @ex(
        help='List EC2 instances',
        epilog = 'Usage: sshaws list',
        description = 'List EC2 Instances in default regions')
    def list(self):
        self.app.log.debug("list command...")
        instance_collections = self.app.ec2.get_instance_collections()

        # create a dataset
        headers = ['Name', 'IP', 'ID', 'Type', 'Region']

        data = []

        for region, ec2_instances in instance_collections.items():
            for ec2_instance in ec2_instances:
                data.append([
                    self.get_ec2_instance_tag_name(ec2_instance.tags), 
                    ec2_instance.public_ip_address, 
                    ec2_instance.id, 
                    ec2_instance.instance_type,
                    region
                ])

        self.app.render(data, headers=headers)

        pass

    def get_ec2_instance_tag_name(self, ec2_tags):
        for tags in ec2_tags:
            if tags["Key"] == 'Name':
                instancename = tags["Value"]
        return instancename