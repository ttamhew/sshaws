from cement import Controller, ex
import os
import boto3
from ..core.exc import SshAwsError
import inquirer
from pathlib import Path
from ..core.ssh_config import SshawsConfigurationObject

class Add(Controller):
    class Meta:
        label = 'add'
        stacked_type = 'embedded'
        stacked_on = 'base'

    instance_collections = {}
    ec2_instance = object
    ec2_instance_region = str
    ssh_user = str
    ssh_port = int
    ssh_key = Path

    @ex(
        help='Add/Modify EC2 instance SSH config',
        epilog = 'If arguments are not provided you will be prompted for them.',
        description = 'Add/Modify a specidifed EC2 instance as a SSH host in your SSH config. If host already exists it will be modified.',
        arguments=[
            ( [ '-n', '--instance-name' ],
              { 'help' : 'EC2 instance name',
                'action'  : 'store',
                'dest' : 'instance_name',
            } ),
            ( [ '-i', '--instance-id' ],
              { 'help' : 'EC2 instance id',
                'action'  : 'store',
                'dest' : 'instance_id',
            } ),
            ( [ '-u', '--user' ],
              { 'help' : 'SSH user',
                'action'  : 'store',
                'dest' : 'ssh_user',
            } ),
            ( [ '-p', '--port' ],
              { 'help' : 'SSH port',
                'action'  : 'store',
                'dest' : 'ssh_port',
            } ),
            ( [ '-k', '--key' ],
              { 'help' : 'SSH public key',
                'action'  : 'store',
                'dest' : 'ssh_key_file',
            } ),
        ],
        aliases=['modify']
        )
    def add(self):

        # Get ec2 instances
        self.instance_collections = self.app.ec2.get_instance_collections()

        # Determine ec2 instance user want to add/modify
        self.ec2_instance = self.determine_ec2_instance()
        self.ec2_instance_region = self.determine_ec2_instance_region(self.ec2_instance)

        # Determine SSH user
        self.ssh_user = self.determine_ssh_user()

        # Determine SSH port
        self.ssh_port = self.determine_ssh_port()

        # Determine SSH key
        self.ssh_key = self.determine_ssh_key()

        self.app.log.debug("Using EC2 instance %s (%s) %s" % (self.ec2_instance.id, self.ec2_instance.public_ip_address, self.app.ec2.get_ec2_instance_tag_name(self.ec2_instance.tags)))
        self.app.log.debug("SSH User %s" % self.ssh_user)
        self.app.log.debug("SSH Port %s" % self.ssh_port)
        self.app.log.debug("SSH Key %s" % self.ssh_key.resolve())

        self.app.connections.add(
            self.app.ec2.get_ec2_instance_tag_name(self.ec2_instance.tags),
            self.ec2_instance.id,
            self.ec2_instance_region,
            self.ec2_instance.public_ip_address,
            self.ssh_user,
            self.ssh_port,
            self.ssh_key
        )

        pass


    def output_success(self, host):
        headers = ['SSH Host', 'EC2 Name', 'AWS Region', 'IP', 'User', 'Port', 'Key']
        data = [
            [
                host,
                self.app.ec2.get_ec2_instance_tag_name(self.ec2_instance.tags),
                self.ec2_instance_region,
                self.ec2_instance.public_ip_address,
                self.ssh_user,
                self.ssh_port,
                self.ssh_key.resolve()
            ]
        ]

        self.app.render(data, headers=headers)

    def determine_ec2_instance(self):
        if self.app.pargs.instance_name is None and self.app.pargs.instance_id is None:
            return self.ask_ec2_instance_selection()
        elif self.app.pargs.instance_id is not None:
            return self.resolve_ec2_by_id(self.app.pargs.instance_id)
        elif self.app.pargs.instance_name is not None:
            return self.resolve_ec2_by_name(self.app.pargs.instance_name)
        else:
            raise SshAwsError("EC2 instance not identified")
        pass


    def determine_ec2_instance_region(self, selected_instance):
        for region, collection in self.instance_collections.items():
            for instance in collection:
                if selected_instance.id == instance.id:
                    return region
        
        raise SshAwsError("EC2 instance region could not be determined")


    def determine_ssh_user(self):
        if self.app.pargs.ssh_user is None:
            return self.ask_ssh_user()
        else:
            return self.app.pargs.ssh_user


    def determine_ssh_port(self):
        if self.app.pargs.ssh_port is None:
            return self.ask_ssh_port()
        else:
            return self.app.pargs.ssh_port


    def determine_ssh_key(self):
        if self.app.pargs.ssh_key_file is None:
            return self.ask_ssk_key()
        else:
            key_file = Path(self.app.pargs.ssh_key_file)
            if key_file.is_file():
                return key_file.resolve()
            else:
                self.app.log.error("SSH key file %s not found." % self.app.pargs.ssh_key_file)
                raise SshAwsError("SSH key file not found.")


    def resolve_ec2_by_name(self, name):
        for region, collection in self.instance_collections.items():
            for instance in collection:
                if self.app.ec2.get_ec2_instance_tag_name(instance.tags) == name:
                    return instance

        self.app.log.error("EC2 instance could not be resolved by name")
        raise SshAwsError("EC2 instance could not be resolved by name") 


    def resolve_ec2_by_id(self, id):
        for region, collection in self.instance_collections.items():
            for instance in collection:
                if instance.id == id:
                    return instance

        self.app.log.error("EC2 instance could not be resolved by id")
        raise SshAwsError("EC2 instance could not be resolved by id")


    def ask_ssk_key(self):
        options = []

        keys = self.get_keys_from_key_dir()

        questions = [
            inquirer.List('ssh_key_file',
            message="SSH key:",
            choices=keys
            ),
        ]

        responses = inquirer.prompt(questions)

        return responses['ssh_key_file']


    def ask_ssh_port(self):
        questions = [
            inquirer.Text('ssh_port',
            message="SSH port:",
            default='22',
            ),
        ]

        responses = inquirer.prompt(questions)
        return responses['ssh_port']


    def ask_ssh_user(self):
        questions = [
            inquirer.Text('ssh_user',
            message="SSH user:",
            default='admin',
            ),
        ]

        responses = inquirer.prompt(questions)
        return responses['ssh_user']


    def ask_ec2_instance_selection(self):
        options = []
        
        for region, collection in self.instance_collections.items():
            for instance in collection:
                options.append((self.app.ec2.get_ec2_instance_tag_name(instance.tags) + " - "  + region, instance))

        questions = [
            inquirer.List('ec2_instance',
            message="Select a EC2 instance:",
            choices=options,
            ),
        ]

        responses = inquirer.prompt(questions)
        return responses['ec2_instance']


    def get_keys_from_key_dir(self):   
        key_list = []

        key_dirpath = Path(self.app.config.get('sshaws', 'ssh_keys_directory'))

        assert(key_dirpath.is_dir(), 'SSH key directory is not a path.')

        for x in key_dirpath.iterdir():
            if x.is_file():
                key_list.append(x.resolve())
            else:
                key_list.append(self.get_keys_from_key_dir(key_dirpath/x))

        return key_list