
from setuptools import setup, find_packages
from sshaws.core.version import get_version

VERSION = get_version()

f = open('README.md', 'r')
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name='sshaws',
    version=VERSION,
    description='SSH AWS Helper Utility',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    author='Matthew Lister',
    author_email='matt.lister@theaccessgroup.com',
    url='https://github.com/johndoe/myapp/',
    license='unlicensed',
    packages=find_packages(exclude=['ez_setup', 'tests*']),
    package_data={'sshaws': ['templates/*']},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        sshaws = sshaws.main:main
    """,
)
