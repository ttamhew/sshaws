<div align=center>
<img src="images/sshaws_logo_25.png" align="center" width="150" alt="Project icon">

**A SSH helper utility to make connecting to EC2 instances a little easier.**
</div>

<hr>
Console application that will sync EC2 public addresses with your local SSH config file.  

## Flows

[Flow Diagram](https://miro.com/app/board/o9J_lSI-zHY=/)

## Installation

```
$ pip install -r requirements.txt

$ pip install setup.py
```

## Development

This project includes a number of helpers in the `Makefile` to streamline common development tasks.

### Environment Setup

The following demonstrates setting up and working with a development environment:

```
### create a virtualenv for development

$ make virtualenv

$ source env/bin/activate


### run sshaws cli application

$ sshaws --help


### run pytest / coverage

$ make test
```


### Releasing to PyPi

Before releasing to PyPi, you must configure your login credentials:

**~/.pypirc**:

```
[pypi]
username = YOUR_USERNAME
password = YOUR_PASSWORD
```

Then use the included helper function via the `Makefile`:

```
$ make dist

$ make dist-upload
```

## Deployments

### Docker

Included is a basic `Dockerfile` for building and distributing `SSH AWS`,
and can be built with the included `make` helper:

```
$ make docker

$ docker run -it sshaws --help
```
