
from pytest import raises
from sshaws.main import SshAwsTest

def test_sshaws():
    # test sshaws without any subcommands or arguments
    with SshAwsTest() as app:
        app.run()
        assert app.exit_code == 0


def test_sshaws_debug():
    # test that debug mode is functional
    argv = ['--debug']
    with SshAwsTest(argv=argv) as app:
        app.run()
        assert app.debug is True


def test_command1():
    # test command1 without arguments
    argv = ['command1']
    with SshAwsTest(argv=argv) as app:
        app.run()
        data,output = app.last_rendered
        assert data['foo'] == 'bar'
        assert output.find('Foo => bar')


    # test command1 with arguments
    argv = ['command1', '--foo', 'not-bar']
    with SshAwsTest(argv=argv) as app:
        app.run()
        data,output = app.last_rendered
        assert data['foo'] == 'not-bar'
        assert output.find('Foo => not-bar')
